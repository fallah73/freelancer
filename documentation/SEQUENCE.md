# Sequence Diagram
## 1 Register
![alt text](images/RegisterSequence.png)
## 2 Login
![alt text](images/LoginSequence.png)
## 3 Create Project
![alt text](images/CreateProjectSequence.png)
## 4 Suggest
![alt text](images/SuggestSequence.png)
## 5 Choose Freelancer
![alt text](images/ChooseFreelancerSequence.png)
