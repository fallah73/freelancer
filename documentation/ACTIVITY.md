# Activity Diagram
## 1 Register
![alt text](images/RegisterActivity.png)
## 2 Login
![alt text](images/LoginActivity.png)
## 3 Create Project
![alt text](images/CreateProjectActivity.png)
## 4 Suggest
![alt text](images/SuggestActivity.png)
## 5 Choose Freelancer
![alt text](images/ChooseFreelancerActivity.png)
## 6 Conversation
![alt text](images/ConversationActivity.png)