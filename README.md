# نرم‌افزار فری‌لنسر
به کمک این نرم‌افزار کارفرما می‌تواند به راحتی در کمترین زمان یک فری‌لنسر خوب را برای انجام پروژه خود انخاب کند.

## وبژگی‌های کلیدی نرم‌افزار
* انتخاب فری‌لنسر در کمترین زمان بر اساس رتبه فری‌لنسر
* مدیریت آسان و افزایش رضایت کارفرما
* انجام پروژه در حداقل زمان ممکن
* مقرون به صرفه بودن

## تحلیل و طراحی پروژه
* در ابتدا به بررسی چند عملکرد اصلی در نرم‌افزار در قالب [سناریوها](documentation/SCENARIO.md) پرداخته‌ایم.
* [نیازمندی‌ها](documentation/REQUIREMENTS.md)
* مدل سازی سناریو با استفاده از  [نمودار مورد کاربرد](documentation/USECASE.md) 
* [نمودار کلاس](documentation/CLASS.md)
* [نمودار توالی](documentation/SEQUENCE.md)
* [نمودار فعالیت](documentation/ACTIVITY.md)
* [نمودار پایگاه داده](documentation/DATABASE.md)

## فازهای توسعه پروژه
* ثبت نام و ورود
* صفحه مشتری
* ساختن پروژه
* پیشنهاد فری‌لنسر
* انتخاب فری‌لنسر
* بخش بحث و گفت‌وگو

## توسعه‌دهنده
|نام و نام خانوادگی|   ID     |
|:------------------:|:---------:|
|      ناهید فلاحی      |  @fallah73  |